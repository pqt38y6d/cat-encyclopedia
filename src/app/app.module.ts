import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainContainerComponent } from './main-container/main-container.component';
import { ItemCardComponent } from './main-container/item-card/item-card.component';
import {HttpClientModule} from '@angular/common/http';
import { ItemInformationComponent } from './main-container/item-information/item-information.component';
import { FilterPipe } from './share/pipes/filter.pipe';
import {FormsModule} from '@angular/forms';
import { NoSelectedItemPageComponent } from './share/components/no-selected-item-page/no-selected-item-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MainContainerComponent,
    ItemCardComponent,
    ItemInformationComponent,
    FilterPipe,
    NoSelectedItemPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
