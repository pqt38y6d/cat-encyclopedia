import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainContainerComponent} from './main-container/main-container.component';
import {ItemInformationComponent} from './main-container/item-information/item-information.component';
import {NoSelectedItemPageComponent} from './share/components/no-selected-item-page/no-selected-item-page.component';


const itemRoutes: Routes = [
  {path: '', component: NoSelectedItemPageComponent},
  {path: ':id', component: ItemInformationComponent}
];

const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: MainContainerComponent, children: itemRoutes},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
