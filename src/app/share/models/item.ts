export class Item {
  id: number;
  name: string;
  shortInfo: string;
  more: string;
  isDeleted?: boolean;
  deletedDate?: Date;
  restoreDate?: Date;
}

