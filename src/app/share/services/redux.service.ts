import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {Item} from '../models/item';
import {RestService} from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class ReduxService {

  constructor(
    private restService: RestService
  ) {
  }


  private itemsSource = new BehaviorSubject<Item[]>([]);
  private deletedItemsSource = new BehaviorSubject<Item[]>([]);
  private currentItemSource = new Subject<Item>();

  items$ = this.itemsSource.asObservable();
  deletedItems$ = this.deletedItemsSource.asObservable();
  currentItem$ = this.currentItemSource.asObservable();

  setInitItems() {
    this.restService.getItemsList().subscribe(items => {
      const deletedItems = this.deletedItemsFromLocalStorage();
      this.replaceWithDeletedItems(deletedItems, items);
      this.deletedItemsSource.next(deletedItems);
      this.itemsSource.next(items);
    });
  }

  setCurrentItem(item: Item) {
    this.currentItemSource.next(item);
  }

  deletedItemsFromLocalStorage(): Item[] {
    if (JSON.parse(localStorage.getItem('deletedItems'))) {
      return JSON.parse(localStorage.getItem('deletedItems'));
    } else {
      return [];
    }
  }


  replaceWithDeletedItems(deletedItems: Item[], items: Item[]) {
    if (deletedItems.length) {
      for (let i = 0; i < deletedItems.length; i++) {
        for (let j = 0; j < items.length; j++) {
          if (items[j].id === deletedItems[i].id) {
            items.splice(items.indexOf(items[j]), 1);
          }
        }
      }
      return items;
    } else {
      return;
    }
  }

}
