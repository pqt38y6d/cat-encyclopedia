import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ItemInformation} from '../models/item-inf';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  BASE_URL = '/tz20';

  constructor(private httpClient: HttpClient) { }

  getItemsList() {
    return this.httpClient.get(`${this.BASE_URL}/list.json`).pipe(
      map(item => {
        // @ts-ignore
        return item.data;
      })
    );
  }

  getItemInformationById(id): Observable<ItemInformation> {
    return this.httpClient.get<ItemInformation>(`${this.BASE_URL}/cats/${id}.json`);
  }

}
