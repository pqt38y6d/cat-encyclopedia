import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-selected-item-page',
  templateUrl: './no-selected-item-page.component.html',
  styleUrls: ['./no-selected-item-page.component.scss']
})
export class NoSelectedItemPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
