import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../share/services/rest.service';
import {ItemInformation} from '../../share/models/item-inf';
import {map} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {ReduxService} from '../../share/services/redux.service';

import {Item} from '../../share/models/item';

@Component({
  selector: 'app-item-information',
  templateUrl: './item-information.component.html',
  styleUrls: ['./item-information.component.scss']
})
export class ItemInformationComponent implements OnInit, OnDestroy {

  itemInf: ItemInformation;
  getItems: Subscription;
  currentItem: Item;
  items: Item[];


  constructor(private activatedRoute: ActivatedRoute,
              private restService: RestService,
              private reduxService: ReduxService,
              private router: Router) {
  }

  ngOnInit() {
    this.setItems();
    this.setCurrentItem();
  }

  setCurrentItem() {
    this.activatedRoute.params.pipe(
      map(params => {
        return +params.id;
      })
    ).subscribe(id => {
      this.restService.getItemInformationById(id).subscribe(itemData => {
        this.itemInf = itemData;
        this.currentItem = this.items.find(item => item.id === id);
        if (!this.currentItem) {
          this.router.navigate(['']);
        }
        this.reduxService.setCurrentItem(this.currentItem);
      });
    });
  }

  setItems() {
    this.getItems = this.reduxService.items$.subscribe(items => {
      this.items = items;
    });
  }

  ngOnDestroy() {
    if (this.getItems) {
      this.getItems.unsubscribe();
    }
  }


}

