import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Item} from '../../share/models/item';
import {ReduxService} from '../../share/services/redux.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit, OnDestroy {

  @Input() item: Item;
  currentItem: Item;

  getItems: Subscription;
  getDeletedItems: Subscription;
  getCurrentItem: Subscription;

  constructor(private reduxService: ReduxService,
              private router: Router) {
  }

  ngOnInit() {
    this.setCurrentItem();
  }

  setCurrentItem() {
    this.getCurrentItem = this.reduxService.currentItem$.subscribe(currentItem => {
      this.currentItem = currentItem;
    });
  }

  deleteItem(item: Item) {
    item.isDeleted = true;
    delete item.restoreDate;
    item.deletedDate = new Date();
    this.getItems = this.reduxService.items$.subscribe(items => {
      items.splice(items.indexOf(item), 1);
    });
    this.getDeletedItems = this.reduxService.deletedItems$.subscribe(deletedItems => {
      deletedItems.push(item);
    });
    this.addDeletedItemToLocalStorage(item);
    if ( this.currentItem && this.currentItem.id === item.id) {
      this.router.navigate(['']);
    }
  }

  addDeletedItemToLocalStorage(item: Item) {
    let deletedItems: Item[];
    if (!JSON.parse(localStorage.getItem('deletedItems'))) {
      deletedItems = [];
    } else {
      deletedItems = JSON.parse(localStorage.getItem('deletedItems'));
    }
    deletedItems.push(item);
    localStorage.setItem('deletedItems', JSON.stringify(deletedItems));
  }

  restoreItem(item: Item) {
    item.isDeleted = false;
    delete item.deletedDate;
    item.restoreDate = new Date();
    this.getItems = this.reduxService.items$.subscribe(items => {
      items.push(item);
    });
    this.getDeletedItems = this.reduxService.deletedItems$.subscribe(deletedItems => {
      deletedItems.splice(deletedItems.indexOf(item), 1);
    });
    this.removeItemFromLocalStorage(item);
  }

  removeItemFromLocalStorage(item: Item) {
    const deletedItems: Item[] = JSON.parse(localStorage.getItem('deletedItems'));
    deletedItems.splice(deletedItems.indexOf(item), 1);
    localStorage.setItem('deletedItems', JSON.stringify(deletedItems));
  }

  ngOnDestroy(): void {
    if (this.getItems) {
      this.getItems.unsubscribe();
    }
    if (this.getDeletedItems) {
      this.getDeletedItems.unsubscribe();
    }
    if (this.getCurrentItem) {
      this.getCurrentItem.unsubscribe();
    }
  }

}
