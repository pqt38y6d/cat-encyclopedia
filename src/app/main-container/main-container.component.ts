import {Component, OnDestroy, OnInit} from '@angular/core';
import {RestService} from '../share/services/rest.service';
import {Item} from '../share/models/item';
import {Subscription} from 'rxjs';
import {ReduxService} from '../share/services/redux.service';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss']
})
export class MainContainerComponent implements OnInit, OnDestroy {

  items: Item[];
  deletedItems: Item[];
  searchText: string;
  getItems: Subscription;
  getDeletedItems: Subscription;

  constructor(private restService: RestService,
              private reduxService: ReduxService) {
  }

  ngOnInit() {
    this.reduxService.setInitItems();
    this.getItems = this.reduxService.items$.subscribe(items => {
      this.items = items;
    });
    this.getDeletedItems = this.reduxService.deletedItems$.subscribe(deletedItems => {
      this.deletedItems = deletedItems;
    });
  }

  ngOnDestroy(): void {
    if (this.getItems) {
      this.getItems.unsubscribe();
    }
    if (this.getDeletedItems) {
      this.getDeletedItems.unsubscribe();
    }
  }
}
